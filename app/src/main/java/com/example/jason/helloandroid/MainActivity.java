package com.example.jason.helloandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = false;

        int id = item.getItemId();

        switch(id)
        {
            case R.id.hello2:
                this.onOptionHello2Selected();
                handled = true;
                break;
            case R.id.hello3:
                this.onOptionHello3Selected();
                handled = true;
                break;
            default:
                handled = super.onOptionsItemSelected(item);
                break;
        }

        return handled;
    }

    private void onOptionHello2Selected()
    {
        Intent intent = new Intent(this, Hello2Activity.class);
        startActivity(intent);
    }

    private void onOptionHello3Selected()
    {
        Intent intent = new Intent(this, Hello3Activity.class);
        startActivity(intent);
    }
}
